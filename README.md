
# txLogger

Store your transactions from your card into DynamoDB to be processed by another application. DynamoDB has some nice stream functionality to be able to process the data in real time




## License

[MIT](https://choosealicense.com/licenses/mit/)


## Requirements
- Free Account with freecurrencyapi 
- Credit Card with Investec (Programmable Banking)
- AWS Account
## Features

- Gather elements from the api call from Investec and store them in a nice, noSQL database
- Complete serverless using AWS lambda but easily cloud agnostic
- Lambda will only be called when you swipe. so it will cost next to nothing
- Written in python 3




## Authors

- [@muhammadebrahim](https://www.linkedin.com/in/muhammad-ebrahim-60214921/)




## Contributing

Contributions are always welcome!

Please create a pull request and be respectful.


## Documentation

Code is clearly commented and installation instructions below.


## Installation

- Start by creating a new Lambda Function. I typically use Python 3.8 and arm64
- Link a trigger to the lambda function and select API Gateway and select REST with no auth (or you can choose to use auth if you like)
- Zip the repo root folder and upload it as the function
- Create a new table in DynamoDB
- Fill in the FreecurrencyAPI and DynamoDB table you created
- You will need to add Policies to read and write to dynamoDB to your Lambda's IAM Role
- Make sure to add main.js sample code to your programmable card (change the URL in POST to your API Gateway URL)

Thats it. Pretty simple to get going
## Tech Stack

**Serverless:** AWS Lambda (Python 3.8)

