import json
import boto3
import uuid
import requests

freecurrencyapi="******"
dynamoTable="****"

def lambda_handler(event, context):
    client = boto3.resource('dynamodb')
    # TODO implement
    
    table = client.Table(dynamoTable)
    print(table.table_status)
    
    body = json.loads(event["body"])
    
    txid = str(uuid.uuid4())
    merchant = body["merchant"]["name"]
    amount = body["centsAmount"]
    currency = body["currencyCode"]
    currency = currency.upper()
    dateTime=body["dateTime"]
    cardid = body["card"]["id"]
    
    if currency != "ZAR":
        url = "https://freecurrencyapi.net/api/v2/latest?apikey="+freecurrencyapi+"&base_currency=ZAR"
        rates = requests.get(url)
        rates=json.loads(rates.text)
        for r in rates["data"]:
            if r == currency:
                amountOrig=amount
                amount=amount*1.025 # Adding currency conversion fees
                amount=round(amount/float(rates["data"][r]),0)
                amount=int(amount)
                currencyOrig=currency
                currency="ZAR"
    
    table.put_item(Item= {'executionId': txid, 'card': cardid, 'merchant': merchant, 'cents': amount, 'currency': currency, 'dateTime': dateTime})
    
    return {
        'statusCode': 200,
        'body': json.dumps('Transaction Logged!')
    }


